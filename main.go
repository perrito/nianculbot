// File: main.go
package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"gopkg.in/telegram-bot-api.v4"

	sparta "github.com/mweagle/Sparta"
	spartaCF "github.com/mweagle/Sparta/aws/cloudformation"
	spartaEvents "github.com/mweagle/Sparta/aws/events"
	"github.com/sirupsen/logrus"
)

// TelegramBotAPI should be set to your bot API Key through `-ldflags '-X main.TelegramBotAPI=XXXXX'`
var TelegramBotAPI = ""

// TelegramRequest holds the fields that come inside a telegram request passed through AWS
// [APIGateway](https://aws.amazon.com/api-gateway/)
type TelegramRequest struct {
	spartaEvents.APIGatewayEnvelope
	Body tgbotapi.Update `json:"body"`
}

// isCommand returns true/false depending of the passed string being a command
// or not and if it is it will also return the command and arguments.
func isCommand(message string) (bool, string, string) {
	is := strings.HasPrefix(message, "/")
	if !is {
		return is, "", ""
	}

	parts := strings.SplitN(message, " ", 2)
	if len(parts) == 1 {
		return is, parts[0], ""
	}

	return is, parts[0], parts[1]
}

const (
	// HelpCommand is the command to ask for usage of the bot.
	HelpCommand = "/help"
	// StartCommand is an alias of HelpCommand.
	StartCommand = "/start"
	// MeCommand performs a dumb action to test the bot.
	MeCommand = "/me"
)

// handle will try to return a response for a given command.
func handle(command, name, args string) (string, bool) {
	resultMessage := ""
	resultIsReply := false
	switch command {
	case HelpCommand, StartCommand:
		resultMessage = "The available commands are: /help /start /me"
		resultIsReply = false
	case MeCommand:
		resultMessage = fmt.Sprintf("*%s %s*", name, args)
		resultIsReply = false
	default:
		resultMessage = fmt.Sprintf("I do not understand command %q 🤷‍♀", command)
		resultIsReply = true
	}
	return resultMessage, resultIsReply
}

// chatty handles telegram messages relayed trough APIGateway.
func chatty(ctx context.Context,
	gatewayEvent *TelegramRequest) (string, error) {
	u := gatewayEvent.Body
	logger, loggerOk := ctx.Value(sparta.ContextKeyLogger).(*logrus.Logger)
	if !loggerOk {
		return "cannot get a logger", nil
	}

	bot, err := tgbotapi.NewBotAPI(TelegramBotAPI)
	if err != nil {
		logger.WithFields(logrus.Fields{
			"Event": gatewayEvent,
		}).Error(err)
		return "cannot create new bot", err
	}

	message := fmt.Sprintf("I don't know what to make of: %q 🤷‍♀️", u.Message.Text)
	isReply := true
	if ok, command, args := isCommand(u.Message.Text); ok {
		message, isReply = handle(command, u.Message.Chat.UserName, args)
	}

	msg := tgbotapi.NewMessage(u.Message.Chat.ID, message)

	if isReply {
		msg.ReplyToMessageID = u.Message.MessageID
	}

	bot.Send(msg)
	return "", nil
}

// main all comments by [Matt Weagle](https://twitter.com/mweagle) author of gosparta.io, special
// thanks to him
func main() {

	// Create a new API Gateway stage that's eligible for a deployment. A stage
	// is a snapshot of the public routes available for an API-G deployment
	apiStage := sparta.NewStage("v1")

	// Create an API Gateway RestAPI resource and associate it with the deployable
	// stage
	// Ref: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-restapi.html
	apiGateway := sparta.NewAPIGateway("NianculBot", apiStage)

	// This allows the URLs to be accessed via AJAX Requests
	apiGateway.CORSOptions = &sparta.CORSOptions{
		Headers: map[string]interface{}{
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key",
			"Access-Control-Allow-Methods": "*",
		},
	}

	// Transform an AWS go-compliant lambda signature into a deployable Sparta
	// https://godoc.org/github.com/mweagle/Sparta#LambdaAWSInfo struct. This
	// struct allows us to associate the lambda function with the API Gateway
	// URL resource
	lambdaFn := sparta.HandleAWSLambda("telegram",
		chatty,
		sparta.IAMRoleDefinition{})

	// Create an API Gateway resource that routes /v1/chat to our lambda
	// function. This associates an API Gateway Integration request with the
	// target lambda function.
	// Ref: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-apigateway-resource.html
	apiGatewayResource, _ := apiGateway.NewResource("/chat", lambdaFn)

	// Once the integration request is established, define the specific
	// HTTP methods available on that request path. Our bot only responds to
	// POST. It also only returns two different status codes (200, 500). Reducing
	// the set of eligible HTTP status codes returned from the function call
	// reduces the set of regular expressions applied to the response body.
	// This improves performance, reduces the provision time, and minimizes
	// the overall CloudFormation stack size.
	// Ref: https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-integration-settings-integration-response.html
	apiMethod, apiMethodErr := apiGatewayResource.NewMethod("POST",
		http.StatusOK,
		http.StatusInternalServerError)
	if nil != apiMethodErr {
		panic("Failed to create /chat resource: " + apiMethodErr.Error())
	}

	// To minimize the number of API Gateway Mapping templates and the
	// overall size and time to provision of our stack, we'll limit the
	// API Gateway route to only accept application/json data provided over
	// an HTTP POST
	// Ref: https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-mapping-template-reference.html
	apiMethod.SupportedRequestContentTypes = []string{"application/json"}

	// Create the slice of lambda functions that define this service
	lambdaFunctions := []*sparta.LambdaAWSInfo{lambdaFn}

	// Create a uniquely named CloudFormation stack for this service. This
	// utility function allows multiple developers to provision the same service
	// in a single AWS account
	stackName := spartaCF.UserScopedStackName("NianculBot")

	// Delegate to Sparta to handle cross compiling, packaging, and managing the
	// service.
	sparta.Main(stackName,
		"Core of the Niancul Chat Bot for Catering Barbecues",
		lambdaFunctions,
		apiGateway,
		nil)
}
