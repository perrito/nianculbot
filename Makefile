export NIANCULBOTAPI := $(NIANCULBOTAPI)
export S3BUCKET := $(S3BUCKET)

.PHONY provision:
provision:
	go run main.go provision --ldflags "-X main.TelegramBotAPI=$(NIANCULBOTAPI)" --s3Bucket $(S3BUCKET)